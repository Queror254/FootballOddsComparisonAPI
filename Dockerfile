FROM ghcr.io/puppeteer/puppeteer:22.1.0 

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true \
    PUPPETEER_EXECUTABLE_PATH=/usr/bin/google-chrome-stable

WORKDIR /usr/src/app

COPY package*.json yarn.lock ./
RUN yarn install --production 
COPY . .
CMD ["node","build/server.js"]